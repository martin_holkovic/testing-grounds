// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_SetFocus.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

void UBTService_SetFocus::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	auto ai = OwnerComp.GetAIOwner();

	if (!ai) return;

	auto board = OwnerComp.GetBlackboardComponent();

	if (!board) return;

	auto target = board->GetValueAsObject(FocalTargetKey.SelectedKeyName);

	if (!target) return;

	auto targetActor = Cast<AActor>(target);

	if (!targetActor) return;

	ai->SetFocus(targetActor);
}
