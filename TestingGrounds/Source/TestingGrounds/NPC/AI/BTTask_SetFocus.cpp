// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_SetFocus.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

EBTNodeResult::Type UBTTask_SetFocus::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto blackBoard = OwnerComp.GetBlackboardComponent();
	auto ai = OwnerComp.GetAIOwner();

	if (!ensure(ai))
		return EBTNodeResult::Failed;

	auto target = blackBoard->GetValueAsObject(FocalTargetSelector.SelectedKeyName);

	if (!target)
		return EBTNodeResult::Succeeded;

	auto targetActor = Cast<AActor>(target);

	if (!targetActor)
		return EBTNodeResult::Succeeded;

	ai->SetFocus(targetActor);

	return EBTNodeResult::Succeeded;
}
