// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_UpdateLastLocation.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API UBTService_UpdateLastLocation : public UBTService
{
	GENERATED_BODY()
	
public:

	void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	UPROPERTY(EditAnywhere)
	struct FBlackboardKeySelector FocalTargetKey;

	UPROPERTY(EditAnywhere)
	struct FBlackboardKeySelector LastSeenLocationKey;
};
