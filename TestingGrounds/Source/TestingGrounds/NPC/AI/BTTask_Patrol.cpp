// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_Patrol.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "PatrolRoute.h"

EBTNodeResult::Type UBTTask_Patrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto patrolGuard = OwnerComp.GetAIOwner()->GetPawn();
	auto patrolRoute = patrolGuard->FindComponentByClass<UPatrolRoute>();
	
	if (!ensure(patrolRoute))
		return EBTNodeResult::Failed;

	auto patrolPoints = patrolRoute->GetPatrolPoints();

	if (patrolPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("PatrolRoute component is missing patrol points."))
		return EBTNodeResult::Failed;
	}

	auto blackBoard = OwnerComp.GetBlackboardComponent();
	auto index = blackBoard->GetValueAsInt(IndexKeySelector.SelectedKeyName);
	index = ++index % patrolPoints.Num();

	blackBoard->SetValueAsInt(IndexKeySelector.SelectedKeyName, index);

	auto nextPoint = patrolPoints[index];

	blackBoard->SetValueAsObject(WaypointKeySelector.SelectedKeyName, nextPoint);

	return EBTNodeResult::Succeeded;
}
